package gitlab.adrian.publictransport.domain.lines;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class Lines {

    String lines;


    public static Lines generate(LinesDto linesDto) {
        Lines lines = new Lines();
        lines.setLines(linesDto.getLines());
        return lines;
    }
}
