package gitlab.adrian.publictransport.domain.lines.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;

@Builder
@Getter
@NonNull
@Value
@JsonDeserialize(builder = GenerateLinesRequestCommand.GenerateLinesRequestCommandBuilder.class)
public
class GenerateLinesRequestCommand {

    String busStopId;
    String busStopNrSlupka;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPOJOBuilder(withPrefix = "")
    public static class GenerateLinesRequestCommandBuilder {
    }
}
