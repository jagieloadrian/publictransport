package gitlab.adrian.publictransport.domain.lines;


import gitlab.adrian.publictransport.domain.lines.request.GenerateLinesRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LinesFacade {

    private final GenerateLines generateLines;

    public List<LinesDto> generateLinesFromStop(GenerateLinesRequestCommand generateLinesRequestCommand) {
        return generateLines.generateLinesFromStop(generateLinesRequestCommand);
    }

}
