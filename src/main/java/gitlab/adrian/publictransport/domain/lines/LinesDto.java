package gitlab.adrian.publictransport.domain.lines;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
public
class LinesDto {

    @NonNull
    String lines;
}
