package gitlab.adrian.publictransport.domain.lines;

import gitlab.adrian.publictransport.domain.lines.request.CreateLinesCommand;

public class LinesDtoMapper {

    public static LinesDto linesDto(CreateLinesCommand createLinesCommand) {
        return LinesDto.builder().
                lines(createLinesCommand.getLine())
                .build();
    }
}
