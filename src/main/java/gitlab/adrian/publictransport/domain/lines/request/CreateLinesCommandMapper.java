package gitlab.adrian.publictransport.domain.lines.request;

import java.util.Map;

public class CreateLinesCommandMapper {

    public static CreateLinesCommand createLinesCommand(Map<String, String> mapaLinii) {
        return CreateLinesCommand.builder()
                .line((ifNull(mapaLinii, "linia")))
                .build();
    }

    private static String ifNull(Map<String, String> mapaLinii, String linia) {
        if (mapaLinii.get(linia).equals("null")) {
            return "0";
        }
        return mapaLinii.get(linia);
    }
}
