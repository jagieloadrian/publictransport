package gitlab.adrian.publictransport.domain.lines.request;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
@NonNull
public class CreateLinesCommand {

    private String line;
}
