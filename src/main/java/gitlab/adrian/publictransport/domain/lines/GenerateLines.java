package gitlab.adrian.publictransport.domain.lines;

import gitlab.adrian.publictransport.domain.lines.request.GenerateLinesRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
class GenerateLines {

    private final LinesRetrievalRestClient linesRetrievalRestClient;

    @Transactional
    public List<LinesDto> generateLinesFromStop(GenerateLinesRequestCommand generateLinesRequestCommand) {
        return linesRetrievalRestClient.getLines(generateLinesRequestCommand);
    }
}
