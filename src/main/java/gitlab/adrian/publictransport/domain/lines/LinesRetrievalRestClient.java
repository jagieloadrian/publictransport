package gitlab.adrian.publictransport.domain.lines;

import gitlab.adrian.publictransport.domain.lines.request.GenerateLinesRequestCommand;

import java.util.List;

public interface LinesRetrievalRestClient {

    List<LinesDto> getLines(GenerateLinesRequestCommand generateLinesRequestCommand);
}
