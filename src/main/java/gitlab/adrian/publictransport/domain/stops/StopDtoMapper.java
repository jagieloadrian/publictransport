package gitlab.adrian.publictransport.domain.stops;

import gitlab.adrian.publictransport.domain.stops.request.CreateStopsCommand;
import org.springframework.stereotype.Component;

@Component
public class StopDtoMapper {

    public static StopDto stopDto(CreateStopsCommand createStopsCommand) {
        return StopDto.builder()
                .zespol(createStopsCommand.getZespol())
                .slupek(String.valueOf(createStopsCommand.getSlupek()))
                .nazwaZespolu(createStopsCommand.getNazwaZespolu())
                .idUlicy(createStopsCommand.getIdUlicy())
                .szerGeo(createStopsCommand.getSzerGeo())
                .dlugGeo(createStopsCommand.getDlugGeo())
                .kierunek(createStopsCommand.getKierunek())
                .obowiazujeOd(createStopsCommand.getObowiazujeOd())
                .build();
    }
}
