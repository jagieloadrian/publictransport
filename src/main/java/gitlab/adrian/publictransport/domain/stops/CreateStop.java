package gitlab.adrian.publictransport.domain.stops;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
class CreateStop {

    private final StopRetrievalRestClient stopRetrievalRestClient;
    private final StopRetrievalClient stopRetrievalClient;

    @Transactional
    public List<StopDto> getStops() {
        List<StopDto> list = stopRetrievalRestClient.getStops();
        for (StopDto stopDto : list) {
            saveStop(stopDto);
        }
        return list;
    }

    public List<Stop> getStopsByNazwaZespolu(String nazwaZespolu) {
        return stopRetrievalClient.getByNazwaZespolu(nazwaZespolu);
    }

    Stop getStopByNazwaZespoluAndSlupek(String nazwaZespolu, String slupek) {
        return stopRetrievalClient.findByNazwaZespoluAndSlupek(nazwaZespolu, slupek);
    }

    private void saveStop(StopDto stopDto) {
        Stop stop = Stop.createStop(stopDto);
        stopRetrievalClient.saveStop(stop);
    }

    List<Stop> getAllStops() {
        return stopRetrievalClient.getAllStops();
    }
}
