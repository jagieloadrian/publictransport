package gitlab.adrian.publictransport.domain.stops.request;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
@NonNull
public class CreateStopsCommand {

    private String zespol;
    private int slupek;
    private String nazwaZespolu;
    private int idUlicy;
    private double szerGeo;
    private double dlugGeo;
    private String kierunek;
    private String obowiazujeOd;
}
