package gitlab.adrian.publictransport.domain.stops;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StopFacade {

    private final CreateStop createStop;

    public List<StopDto> pullStops() {
        return createStop.getStops();
    }


    public Stop getStopByNazwaZespoluAndSlupek(String nazwaZespolu, String slupek) {
        return createStop.getStopByNazwaZespoluAndSlupek(nazwaZespolu, slupek);
    }

    public List<Stop> getStopsByNazwaZespolu(String nazwaZespolu) {
        return createStop.getStopsByNazwaZespolu(nazwaZespolu);
    }

    public List<Stop> gatAllStops() {
        return createStop.getAllStops();
    }
}
