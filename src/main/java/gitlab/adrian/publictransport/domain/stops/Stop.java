package gitlab.adrian.publictransport.domain.stops;


import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Table(name = "Stops")
public class Stop {

    @Id
    @Setter(value = AccessLevel.PRIVATE)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "stop_sequence")
    @SequenceGenerator(name = "stop_sequence")
    private long id;

    private String zespol;
    private String slupek;
    private String nazwaZespolu;
    private int idUlicy;
    private double szerGeo;
    private double dlugGeo;
    private String kierunek;
    private String obowiazujeOd;

    public static Stop createStop(StopDto stopDto) {
        Stop stop = new Stop();
        stop.setZespol(stopDto.getZespol());
        stop.setSlupek(stopDto.getSlupek());
        stop.setNazwaZespolu(stopDto.getNazwaZespolu());
        stop.setIdUlicy(stopDto.getIdUlicy());
        stop.setSzerGeo(stopDto.getSzerGeo());
        stop.setDlugGeo(stopDto.getDlugGeo());
        stop.setKierunek(stopDto.getKierunek());
        stop.setObowiazujeOd(stopDto.getObowiazujeOd());
        return stop;
    }

}
