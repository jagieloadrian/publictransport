package gitlab.adrian.publictransport.domain.stops;

import java.util.List;

public interface StopRetrievalClient {

    List<Stop> getByNazwaZespolu(String nazwaZespolu);

    void saveStop(Stop stop);

    Stop findByNazwaZespoluAndSlupek(String nazwaZespolu, String slupek);

    List<Stop> getAllStops();
}
