package gitlab.adrian.publictransport.domain.stops;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Getter
@Builder
public class StopDto {

    @NonNull
    String zespol;
    @NonNull
    String slupek;
    @NonNull
    String nazwaZespolu;
    @NonNull
    int idUlicy;
    @NonNull
    double szerGeo;
    @NonNull
    double dlugGeo;
    @NonNull
    String kierunek;
    @NonNull
    String obowiazujeOd;
}
