package gitlab.adrian.publictransport.domain.stops.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;

@Builder
@Getter
@NonNull
@Value
@JsonDeserialize(builder = GetStopRequestCommand.GetStopRequestCommandBuilder.class)
public class GetStopRequestCommand {

    String zespol;
    int slupek;
    String nazwaZespolu;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPOJOBuilder(withPrefix = "")
    public static class GetStopRequestCommandBuilder {
    }
}
