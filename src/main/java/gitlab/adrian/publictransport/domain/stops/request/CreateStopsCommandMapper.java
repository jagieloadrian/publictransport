package gitlab.adrian.publictransport.domain.stops.request;

import java.util.Map;

public class CreateStopsCommandMapper {

    public static CreateStopsCommand createStopsCommand(Map<String, String> mapaStopow) {
        return CreateStopsCommand.builder()
                .zespol((ifNull(mapaStopow, "zespol")))
                .slupek(Integer.parseInt(ifNull(mapaStopow, "slupek")))
                .nazwaZespolu(ifNull(mapaStopow, "nazwa_zespolu"))
                .idUlicy(Integer.parseInt(ifNull(mapaStopow, "id_ulicy")))
                .dlugGeo(Double.parseDouble(ifNull(mapaStopow, "dlug_geo")))
                .szerGeo(Double.parseDouble(ifNull(mapaStopow, "szer_geo")))
                .kierunek(ifNull(mapaStopow, "kierunek"))
                .obowiazujeOd(ifNull(mapaStopow, "obowiazuje_od"))
                .build();
    }

    private static String ifNull(Map<String, String> mapaStopow, String pole) {
        if (mapaStopow.get(pole).equals("null")) {
            return "0";
        }
        return mapaStopow.get(pole);
    }
}
