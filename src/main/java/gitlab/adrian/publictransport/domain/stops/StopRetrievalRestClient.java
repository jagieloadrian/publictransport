package gitlab.adrian.publictransport.domain.stops;

import java.util.List;


public interface StopRetrievalRestClient {

    List<StopDto> getStops();

}
