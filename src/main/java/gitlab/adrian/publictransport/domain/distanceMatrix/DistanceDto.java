package gitlab.adrian.publictransport.domain.distanceMatrix;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class DistanceDto {
    String originAddress;
    String destinationAddress;
    String time;
    String timeInSeconds;
    String distance;
    String distanceInUnits;
}
