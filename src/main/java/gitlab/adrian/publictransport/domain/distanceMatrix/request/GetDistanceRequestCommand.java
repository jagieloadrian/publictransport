package gitlab.adrian.publictransport.domain.distanceMatrix.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;

@Builder
@Getter
@NonNull
@Value
@JsonDeserialize(builder = GetDistanceRequestCommand.GetDistanceRequestCommandBuilder.class)
public class GetDistanceRequestCommand {

    String nazwaZespolu;
    String busStopNrSlupka;
    String line;

    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPOJOBuilder(withPrefix = "")
    public static class GetDistanceRequestCommandBuilder {
    }
}
