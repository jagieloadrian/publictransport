package gitlab.adrian.publictransport.domain.distanceMatrix;

import gitlab.adrian.publictransport.domain.distanceMatrix.request.GetDistanceRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public
class DistanceMatrixFacade {

    private final GenerateDistanceMatrix generateDistanceMatrix;

    public List<Distance> getList(GetDistanceRequestCommand getDistanceRequestCommand) {
        return generateDistanceMatrix.generateDistance(getDistanceRequestCommand);
    }
}
