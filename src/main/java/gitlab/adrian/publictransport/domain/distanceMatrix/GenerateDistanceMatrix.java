package gitlab.adrian.publictransport.domain.distanceMatrix;

import gitlab.adrian.publictransport.domain.distanceMatrix.request.GetDistanceRequestCommand;
import gitlab.adrian.publictransport.domain.distanceMatrix.request.GetDistanceRestCommand;
import gitlab.adrian.publictransport.domain.stops.Stop;
import gitlab.adrian.publictransport.domain.stops.StopFacade;
import gitlab.adrian.publictransport.domain.vehicle.Vehicle;
import gitlab.adrian.publictransport.domain.vehicle.VehicleFacade;
import gitlab.adrian.publictransport.domain.vehicle.request.CreateVehicleRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
class GenerateDistanceMatrix {

    private final StopFacade stopFacade;
    private final VehicleFacade vehicleFacade;
    private final DistanceRetrievalRestClient distanceRetrievalRestClient;

    @Transactional
    public List<Distance> generateDistance(GetDistanceRequestCommand getDistanceRequestCommand) {
        GetDistanceRestCommand getDistanceRestCommand = getDistanceRestCommandMapper(getDistanceRequestCommand.getNazwaZespolu()
                , getDistanceRequestCommand.getBusStopNrSlupka()
                , getDistanceRequestCommand.getLine());
        return distanceRetrievalRestClient.getDistance(getDistanceRestCommand)
                .stream().map(Distance::generate)
                .collect(Collectors.toList());
    }

    Stop getStopByNazwaZespoluAndSlupek(String nazwaZespolu, String slupek) {
        return stopFacade.getStopByNazwaZespoluAndSlupek(nazwaZespolu, slupek);
    }

    List<Vehicle> pullVehicleListByLine(String line) {
        return vehicleFacade.pullLineList(createVehicleRequestCommandMapper(line));
    }

    private CreateVehicleRequestCommand createVehicleRequestCommandMapper(String line) {
        return CreateVehicleRequestCommand.builder().line(line).build();
    }

    private GetDistanceRestCommand getDistanceRestCommandMapper(String nazwaZespolu,
                                                                String slupek, String line) {
        return GetDistanceRestCommand.builder().list(pullVehicleListByLine(line))
                .stop(getStopByNazwaZespoluAndSlupek(nazwaZespolu, slupek)).build();
    }
}
