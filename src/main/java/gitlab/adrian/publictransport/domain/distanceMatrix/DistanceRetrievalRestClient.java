package gitlab.adrian.publictransport.domain.distanceMatrix;

import gitlab.adrian.publictransport.domain.distanceMatrix.request.GetDistanceRestCommand;

import java.util.List;

public interface DistanceRetrievalRestClient {
    List<DistanceDto> getDistance(GetDistanceRestCommand getDistanceRestCommand);
}
