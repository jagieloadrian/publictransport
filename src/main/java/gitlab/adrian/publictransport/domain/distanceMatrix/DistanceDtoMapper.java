package gitlab.adrian.publictransport.domain.distanceMatrix;

import gitlab.adrian.publictransport.domain.distanceMatrix.request.CreateDistanceCommand;

public class DistanceDtoMapper {
    public static DistanceDto distanceDto(CreateDistanceCommand createDistanceCommand) {
        return DistanceDto.builder()
                .originAddress(createDistanceCommand.getOriginAddress())
                .destinationAddress(createDistanceCommand.getDestinationAddress())
                .distance(createDistanceCommand.getDistance())
                .distanceInUnits(createDistanceCommand.getDistanceInUnits())
                .time(createDistanceCommand.getTime())
                .timeInSeconds(createDistanceCommand.getTimeInSeconds()).build();
    }
}
