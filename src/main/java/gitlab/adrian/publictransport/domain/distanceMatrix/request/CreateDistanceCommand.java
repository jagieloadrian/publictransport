package gitlab.adrian.publictransport.domain.distanceMatrix.request;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
@NonNull
public class CreateDistanceCommand {
    public String originAddress;
    public String destinationAddress;
    public String time;
    public String timeInSeconds;
    public String distance;
    public String distanceInUnits;
}
