package gitlab.adrian.publictransport.domain.distanceMatrix;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class Distance {
    String originAddress;
    String destinationAddress;
    String time;
    String timeInSeconds;
    String distance;
    String distanceInUnits;


    public static Distance generate(DistanceDto distanceDto) {
        Distance distance = new Distance();
        distance.setOriginAddress(distanceDto.getOriginAddress());
        distance.setDestinationAddress(distanceDto.getDestinationAddress());
        distance.setTime(distanceDto.getTime());
        distance.setTimeInSeconds(distanceDto.getTimeInSeconds());
        distance.setDistance(distanceDto.getDistance());
        distance.setDistanceInUnits(distanceDto.getDistanceInUnits());
        return distance;
    }
}
