package gitlab.adrian.publictransport.domain.distanceMatrix.request;

import gitlab.adrian.publictransport.domain.stops.Stop;
import gitlab.adrian.publictransport.domain.vehicle.Vehicle;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.util.List;

@Builder
@Getter
@NonNull
public class GetDistanceRestCommand {
    Stop stop;
    List<Vehicle> list;

}
