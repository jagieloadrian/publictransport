package gitlab.adrian.publictransport.domain.vehicle;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
public class VehicleDto {

    @NonNull
    String line;
    @NonNull
    double lon;
    @NonNull
    String vehicleNumber;
    @NonNull
    String time;
    @NonNull
    double lat;
    @NonNull
    String brigade;
}
