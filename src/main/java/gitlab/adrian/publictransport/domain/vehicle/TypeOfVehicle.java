package gitlab.adrian.publictransport.domain.vehicle;

public enum TypeOfVehicle {
    BUS(1),
    TRAM(2);

    private int type;

    TypeOfVehicle(int type) {
        this.type = type;
    }
}
