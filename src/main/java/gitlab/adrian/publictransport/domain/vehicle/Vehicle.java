package gitlab.adrian.publictransport.domain.vehicle;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor(access = AccessLevel.PACKAGE)
@Getter
@Setter
@Builder
public class Vehicle {

    String line;
    double lon;
    String vehicleNumber;
    String time;
    double lat;
    String brigade;
    TypeOfVehicle typeOfVehicle;


    public static Vehicle generate(VehicleDto vehicleDto) {
        Vehicle vehicle = new Vehicle();
        vehicle.setLine(vehicleDto.getLine());
        vehicle.setLon(vehicleDto.getLon());
        vehicle.setVehicleNumber(vehicleDto.getVehicleNumber());
        vehicle.setTime(vehicleDto.getTime());
        vehicle.setLat(vehicleDto.getLat());
        vehicle.setBrigade(vehicleDto.getBrigade());
        vehicle.setType(vehicleDto.getLine());
        return vehicle;
    }

    void setType(String line) {
        if (line.length() <= 2) {
            this.typeOfVehicle = TypeOfVehicle.TRAM;
        } else {
            this.typeOfVehicle = TypeOfVehicle.BUS;
        }
    }
}
