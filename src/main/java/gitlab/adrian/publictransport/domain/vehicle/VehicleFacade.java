package gitlab.adrian.publictransport.domain.vehicle;

import gitlab.adrian.publictransport.domain.vehicle.request.CreateVehicleRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class VehicleFacade {

    private final GenerateVehicle generateVehicle;

    public List<Vehicle> pullLineList(CreateVehicleRequestCommand createVehicleRequestCommand) {
        return generateVehicle.generateListVehicle(createVehicleRequestCommand);
    }
}
