package gitlab.adrian.publictransport.domain.vehicle;

import gitlab.adrian.publictransport.domain.vehicle.request.CreateVehicleRequestCommand;

import java.util.List;

public interface VehicleRetrievalRestClient {


    List<VehicleDto> getVehicles(CreateVehicleRequestCommand createVehicleRequestCommand);

}
