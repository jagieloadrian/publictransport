package gitlab.adrian.publictransport.domain.vehicle.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Value;

@Builder
@Getter
@NonNull
@Value
@JsonDeserialize(builder = CreateVehicleRequestCommand.CreateVehicleRequestCommandBuilder.class)
public class CreateVehicleRequestCommand {

    String line;


    @JsonIgnoreProperties(ignoreUnknown = true)
    @JsonPOJOBuilder(withPrefix = "")
    public static class CreateVehicleRequestCommandBuilder {
    }
}

