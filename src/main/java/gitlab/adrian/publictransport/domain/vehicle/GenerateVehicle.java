package gitlab.adrian.publictransport.domain.vehicle;

import gitlab.adrian.publictransport.domain.vehicle.request.CreateVehicleRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class GenerateVehicle {


    private final VehicleRetrievalRestClient vehicleRetrievalRestClient;

    @Transactional
    public List<Vehicle> generateListVehicle(CreateVehicleRequestCommand createVehicleRequestCommand) {
        List<VehicleDto> vehicleDtoList = vehicleRetrievalRestClient.getVehicles(createVehicleRequestCommand);
        return vehicleDtoList.stream().map(Vehicle::generate).collect(Collectors.toList());

    }
}
