package gitlab.adrian.publictransport.domain.vehicle;

import gitlab.adrian.publictransport.api.vehicle.VehicleRequest;
import org.springframework.stereotype.Component;

@Component
public class VehicleDtoMapper {

    public static VehicleDto vehicleDto(VehicleRequest vehicleRequest) {
        return VehicleDto.builder()
                .line(vehicleRequest.getLines())
                .lon(vehicleRequest.getLon())
                .vehicleNumber(vehicleRequest.getVehicleNumber())
                .time(vehicleRequest.getTime())
                .lat(vehicleRequest.getLat())
                .brigade(vehicleRequest.getBrigade())
                .build();
    }
}
