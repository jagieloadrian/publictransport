package gitlab.adrian.publictransport.api.distanceMatrix;

import gitlab.adrian.publictransport.domain.distanceMatrix.DistanceMatrixFacade;
import gitlab.adrian.publictransport.domain.distanceMatrix.request.GetDistanceRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/distance")
@RequiredArgsConstructor
class DistanceController {

    private final DistanceMatrixFacade distanceFacade;


    /*
     * Post mapping, który po podaniu linii i nazwy przystanku oraz nr słupka pokazuje wszytkie pojazdy (z danej linii)
     * oraz odległości pojazdów do przystanku.
     * Dane przystanku pobiera z lokalnej DB*/

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    List<gitlab.adrian.publictransport.domain.distanceMatrix.Distance> getList(@Valid @RequestBody GetDistanceRequestCommand command) {
        GetDistanceRequestCommand getDistanceRequestCommand = getDistanceRequestCommandMapper(command);
        return distanceFacade.getList(getDistanceRequestCommand);
    }

    private GetDistanceRequestCommand getDistanceRequestCommandMapper(GetDistanceRequestCommand getDistanceRequestCommand) {
        return GetDistanceRequestCommand.builder().
                busStopNrSlupka(getDistanceRequestCommand.getBusStopNrSlupka())
                .line(getDistanceRequestCommand.getLine())
                .nazwaZespolu(getDistanceRequestCommand.getNazwaZespolu())
                .build();

    }

}
