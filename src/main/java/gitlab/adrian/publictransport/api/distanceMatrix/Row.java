package gitlab.adrian.publictransport.api.distanceMatrix;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "elements"
})
@JsonDeserialize(builder = Row.RowBuilder.class)
@Builder
public class Row {
    @JsonProperty("elements")
    private List<Element> elements = null;


    @JsonProperty("elements")
    public List<Element> getElements() {
        return elements;
    }

    @JsonProperty("elements")
    public void setElements(List<Element> elements) {
        this.elements = elements;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class RowBuilder {
    }

}
