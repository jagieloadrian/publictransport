package gitlab.adrian.publictransport.api.stop;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;

import java.util.List;

@JsonPropertyOrder({
        "result"
})
@Builder
@JsonDeserialize(builder = ResultList.ResultListBuilder.class)
public class ResultList {

    @JsonProperty("result")
    private List<StopRequestList> result = null;

    @JsonProperty("result")
    public List<StopRequestList> getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(List<StopRequestList> result) {
        this.result = result;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class ResultListBuilder {
    }

}