package gitlab.adrian.publictransport.api.stop;

import gitlab.adrian.publictransport.domain.stops.Stop;
import gitlab.adrian.publictransport.domain.stops.StopDto;
import gitlab.adrian.publictransport.domain.stops.StopFacade;
import gitlab.adrian.publictransport.domain.stops.request.GetStopRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/stops")
@RequiredArgsConstructor
class StopController {

    private final StopFacade stopFacade;

    /*Get mapping, który ściąga i zapisuje do lokalnej DB wszystkie przystanki z warszawy */
    @GetMapping("/pull")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<StopDto> pullStops() {
        return stopFacade.pullStops();
    }


    /*
     * pokazuje wszytkie przystanki (oraz ich dane) wyszukując po nazwie np CENTRUM*/
    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<Stop> getStopsByNazwaZespolu(@Valid @RequestBody GetStopRequestCommand creator) {
        GetStopRequestCommand getStopRequestCommand = createStopRequestCommandMapper(creator);
        String nazwaZespolu = getStopRequestCommand.getNazwaZespolu();
        return stopFacade.getStopsByNazwaZespolu(nazwaZespolu);
    }

    /*pokazuje wszytkie przystanki jakie są w bazie danych */
    @GetMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<Stop> getAllStops() {
        return stopFacade.gatAllStops();
    }

    private GetStopRequestCommand createStopRequestCommandMapper(GetStopRequestCommand command) {
        return GetStopRequestCommand.builder().nazwaZespolu(command.getNazwaZespolu()).build();
    }
}
