package gitlab.adrian.publictransport.api.stop;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "values"
})
@Builder
@JsonDeserialize(builder = StopRequestList.StopRequestListBuilder.class)
public class StopRequestList {

    @JsonProperty("values")
    private List<StopRequest> stopRequests;

    @JsonProperty("values")
    public List<StopRequest> getValues() {
        return stopRequests;
    }

    @JsonProperty("values")
    public void setValues(List<StopRequest> stopRequests) {
        this.stopRequests = stopRequests;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class StopRequestListBuilder {
    }

}