package gitlab.adrian.publictransport.api.lines;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;

import java.util.List;

@JsonPropertyOrder({
        "result"
})
@Builder
@JsonDeserialize(builder = Result.ResultBuilder.class)
public class Result {

    @JsonProperty("result")
    private List<LinesList> result = null;

    @JsonProperty("result")
    public List<LinesList> getResult() {
        return result;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static class ResultBuilder {
    }
}
