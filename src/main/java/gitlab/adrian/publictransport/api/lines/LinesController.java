package gitlab.adrian.publictransport.api.lines;

import gitlab.adrian.publictransport.domain.lines.LinesDto;
import gitlab.adrian.publictransport.domain.lines.LinesFacade;
import gitlab.adrian.publictransport.domain.lines.request.GenerateLinesRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/lines")
@RequiredArgsConstructor
class LinesController {

    private final LinesFacade linesFacade;

/*Post mapping, który pokazuje wszystkie dostępne linie z danego przystanku. Dane mogą być pobrane z
 * */
    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<LinesDto> getLinesFromStop(@Valid @RequestBody GenerateLinesRequestCommand generate) {
        GenerateLinesRequestCommand generateLinesRequestCommand = generateLinesRequestCommandMapper(generate);
        return linesFacade.generateLinesFromStop(generateLinesRequestCommand);
    }

    private GenerateLinesRequestCommand generateLinesRequestCommandMapper(GenerateLinesRequestCommand command) {
        return GenerateLinesRequestCommand.builder()
                .busStopId(command.getBusStopId())
                .busStopNrSlupka(command.getBusStopNrSlupka())
                .build();
    }
}
