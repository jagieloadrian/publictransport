package gitlab.adrian.publictransport.api.lines;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;


@JsonPropertyOrder({
        "value",
        "key"
})
@Builder
@JsonDeserialize(builder = Lines.LinesBuilder.class)
public class Lines {

    @JsonProperty("value")
    private String value;
    @JsonProperty("key")
    private String key;

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }


    @JsonPOJOBuilder(withPrefix = "")
    public static class LinesBuilder {
    }
}
