package gitlab.adrian.publictransport.api.lines;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;

import java.util.List;

@JsonPropertyOrder({
        "values"
})
@Builder
@JsonDeserialize(builder = LinesList.LinesListBuilder.class)
public class LinesList {

    @JsonProperty("values")
    private List<Lines> values = null;

    @JsonProperty("values")
    public List<Lines> getValues() {
        return values;
    }


    @JsonPOJOBuilder(withPrefix = "")
    public static class LinesListBuilder {
    }

}
