package gitlab.adrian.publictransport.api.vehicle;

import gitlab.adrian.publictransport.domain.vehicle.Vehicle;
import gitlab.adrian.publictransport.domain.vehicle.VehicleFacade;
import gitlab.adrian.publictransport.domain.vehicle.request.CreateVehicleRequestCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/publictransport")
@RequiredArgsConstructor
class VehicleController {

    private final VehicleFacade vehicleFacade;

    /*
     * Post mapping który pokazuje liste pojazdów z danej linii */
    @PostMapping("/vehicles")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public List<Vehicle> generateListVehicle(@Valid @RequestBody CreateVehicleRequestCommand creator) {
        CreateVehicleRequestCommand createVehicleRequestCommand = createRequestCommandMapper(creator);
        return vehicleFacade.pullLineList(createVehicleRequestCommand);
    }

    private CreateVehicleRequestCommand createRequestCommandMapper(CreateVehicleRequestCommand command) {
        return CreateVehicleRequestCommand.builder().line(command.getLine()).build();
    }
}
