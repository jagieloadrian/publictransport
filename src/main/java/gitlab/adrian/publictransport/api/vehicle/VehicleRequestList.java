package gitlab.adrian.publictransport.api.vehicle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;

import javax.validation.Valid;
import java.util.List;

@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"result"})
@JsonDeserialize(builder = VehicleRequestList.VehicleRequestListBuilder.class)
public class VehicleRequestList {

    @JsonProperty("result")
    @Valid
    private final List<VehicleRequest> vehicleRequests;

    @JsonProperty("result")
    public List<VehicleRequest> getVehicleRequests() {
        return vehicleRequests;
    }

    public VehicleRequestList(@Valid List<VehicleRequest> vehicleRequests) {
        this.vehicleRequests = vehicleRequests;
    }


    @JsonPOJOBuilder(withPrefix = "")
    public static class VehicleRequestListBuilder {
    }


}
