package gitlab.adrian.publictransport.api.vehicle;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@JsonDeserialize(builder = VehicleRequest.VehicleRequestBuilder.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Lines",
        "Lon",
        "VehicleNumber",
        "Time",
        "Lat",
        "Brigade"
})
public class VehicleRequest {

    @JsonProperty("Lines")
    private String lines;
    @JsonProperty("Lon")
    private Double lon;
    @JsonProperty("VehicleNumber")
    private String vehicleNumber;
    @JsonProperty("Time")
    private String time;
    @JsonProperty("Lat")
    private Double lat;
    @JsonProperty("Brigade")
    private String brigade;

    @JsonPOJOBuilder(withPrefix = "")
    public static class VehicleRequestBuilder {
    }

    public VehicleRequest(String lines, Double lon, String vehicleNumber, String time, Double lat, String brigade) {
        this.lines = lines;
        this.lon = lon;
        this.vehicleNumber = vehicleNumber;
        this.time = time;
        this.lat = lat;
        this.brigade = brigade;
    }


    @JsonProperty("Lines")
    public String getLines() {
        return lines;
    }

    @JsonProperty("Lon")
    public Double getLon() {
        return lon;
    }

    @JsonProperty("VehicleNumber")
    public String getVehicleNumber() {
        return vehicleNumber;
    }

    @JsonProperty("Time")
    public String getTime() {
        return time;
    }

    @JsonProperty("Lat")
    public Double getLat() {
        return lat;
    }

    @JsonProperty("Brigade")
    public String getBrigade() {
        return brigade;
    }

}