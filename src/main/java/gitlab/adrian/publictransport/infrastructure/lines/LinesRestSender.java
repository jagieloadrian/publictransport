package gitlab.adrian.publictransport.infrastructure.lines;

import gitlab.adrian.publictransport.api.lines.Lines;
import gitlab.adrian.publictransport.api.lines.LinesList;
import gitlab.adrian.publictransport.api.lines.Result;
import gitlab.adrian.publictransport.domain.lines.LinesDto;
import gitlab.adrian.publictransport.domain.lines.LinesDtoMapper;
import gitlab.adrian.publictransport.domain.lines.LinesRetrievalRestClient;
import gitlab.adrian.publictransport.domain.lines.request.CreateLinesCommandMapper;
import gitlab.adrian.publictransport.domain.lines.request.GenerateLinesRequestCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
class LinesRestSender implements LinesRetrievalRestClient {

    private final RestTemplate restTemplate;
    private final String getLinesOfStopBaseUrl;
    private final String apiKeyUrl;

    @Autowired
    LinesRestSender(RestTemplate restTemplate,
                    @Value("${api.um.getLinesOfStop.base.url}") String getLinesOfStopBaseUrl,
                    @Value("${api.um.apikey.url}") String apiKeyUrl) {
        this.restTemplate = restTemplate;
        this.getLinesOfStopBaseUrl = getLinesOfStopBaseUrl;
        this.apiKeyUrl = apiKeyUrl;
    }


    @Override
    public List<LinesDto> getLines(GenerateLinesRequestCommand generateLinesRequestCommand) {
        String url = generateURL(generateLinesRequestCommand.getBusStopId(),
                generateLinesRequestCommand.getBusStopNrSlupka());
        Result result = restTemplate.getForObject(url, Result.class);
        assert result != null;
        List<LinesList> linesLists = result.getResult();
        List<Map<String, String>> linesMap = new ArrayList<>();
        for (LinesList linesList : linesLists) {
            List<Lines> anotherResult = linesList.getValues();
            Map<String, String> lines = new HashMap<>();
            for (Lines line : anotherResult) {
                lines.put(line.getKey(), line.getValue());
            }
            linesMap.add(lines);
        }
        return linesMap.stream()
                .map(CreateLinesCommandMapper::createLinesCommand)
                .map(LinesDtoMapper::linesDto)
                .collect(Collectors.toList());
    }

    String generateURL(String busStopId, String busStopNr) {
        String apiKeyUrl = this.apiKeyUrl;
        String baseBusStopId = "&busstopId=";
        String baseBusStopNr = "&busstopNr=";
        return getLinesOfStopBaseUrl + baseBusStopId
                + busStopId + baseBusStopNr
                + busStopNr + apiKeyUrl;
    }
}
