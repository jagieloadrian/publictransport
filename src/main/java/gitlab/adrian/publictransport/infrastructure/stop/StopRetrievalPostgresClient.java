package gitlab.adrian.publictransport.infrastructure.stop;

import gitlab.adrian.publictransport.domain.stops.Stop;
import gitlab.adrian.publictransport.domain.stops.StopRetrievalClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
class StopRetrievalPostgresClient implements StopRetrievalClient {

    private final StopRepository stopRepository;

    @Override
    public List<Stop> getByNazwaZespolu(String nazwaZespolu) {
        return stopRepository.findByNazwaZespolu(nazwaZespolu);
    }

    @Override
    public void saveStop(Stop stop) {
        stopRepository.save(stop);
    }

    @Override
    public Stop findByNazwaZespoluAndSlupek(String nazwaZespolu, String slupek) {
        return stopRepository.findByNazwaZespoluAndSlupek(nazwaZespolu, slupek);
    }

    @Override
    public List<Stop> getAllStops() {
        return stopRepository.findAll();
    }
}
