package gitlab.adrian.publictransport.infrastructure.stop;

import gitlab.adrian.publictransport.api.stop.ResultList;
import gitlab.adrian.publictransport.api.stop.StopRequest;
import gitlab.adrian.publictransport.api.stop.StopRequestList;
import gitlab.adrian.publictransport.domain.stops.StopDto;
import gitlab.adrian.publictransport.domain.stops.StopDtoMapper;
import gitlab.adrian.publictransport.domain.stops.StopRetrievalRestClient;
import gitlab.adrian.publictransport.domain.stops.request.CreateStopsCommandMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
class StopRestSender implements StopRetrievalRestClient {

    private final RestTemplate restTemplate;
    private final String stopsDownloadUrl;

    @Autowired
    StopRestSender(RestTemplate restTemplate,
                   @Value("${api.um.stops.download.url}") String stopsDownloadUrl) {
        this.restTemplate = restTemplate;
        this.stopsDownloadUrl = stopsDownloadUrl;
    }

    @Override
    public List<StopDto> getStops() {
        ResultList result = restTemplate.getForObject(stopsDownloadUrl,
                ResultList.class);
        assert result != null;
        List<StopRequestList> stopsRequestList = result.getResult();
        List<Map<String, String>> stopsMap = new ArrayList<>();
        for (StopRequestList stopRequestList : stopsRequestList) {
            List<StopRequest> anotherResult = stopRequestList.getValues();
            Map<String, String> stops = new HashMap<>();
            for (StopRequest request : anotherResult) {
                stops.put(request.getKey(), request.getValue());
            }
            stopsMap.add(stops);
        }
        return stopsMap.stream()
                .map(CreateStopsCommandMapper::createStopsCommand)
                .map(StopDtoMapper::stopDto)
                .collect(Collectors.toList());
    }
}
