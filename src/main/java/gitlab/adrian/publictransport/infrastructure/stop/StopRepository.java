package gitlab.adrian.publictransport.infrastructure.stop;


import gitlab.adrian.publictransport.domain.stops.Stop;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


interface StopRepository extends JpaRepository<Stop, Long> {

    List<Stop> findByNazwaZespolu(String nazwaZespolu);

    Stop findByNazwaZespoluAndSlupek(String nazwaZespolu, String slupek);

}
