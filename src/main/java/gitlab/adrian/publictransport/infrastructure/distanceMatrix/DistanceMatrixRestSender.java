package gitlab.adrian.publictransport.infrastructure.distanceMatrix;

import gitlab.adrian.publictransport.api.distanceMatrix.Distance;
import gitlab.adrian.publictransport.api.distanceMatrix.DistanceMatrix;
import gitlab.adrian.publictransport.api.distanceMatrix.Duration;
import gitlab.adrian.publictransport.api.distanceMatrix.Element;
import gitlab.adrian.publictransport.api.distanceMatrix.Row;
import gitlab.adrian.publictransport.domain.distanceMatrix.DistanceDto;
import gitlab.adrian.publictransport.domain.distanceMatrix.DistanceDtoMapper;
import gitlab.adrian.publictransport.domain.distanceMatrix.DistanceRetrievalRestClient;
import gitlab.adrian.publictransport.domain.distanceMatrix.request.CreateDistanceCommand;
import gitlab.adrian.publictransport.domain.distanceMatrix.request.GetDistanceRestCommand;
import gitlab.adrian.publictransport.domain.stops.Stop;
import gitlab.adrian.publictransport.domain.vehicle.Vehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
class DistanceMatrixRestSender implements DistanceRetrievalRestClient {

    private final RestTemplate restTemplate;
    private final String baseUrlDistanceMatrix;
    private final String apiKeyGoogleUrl;
    private final String urlDestinationPart;
    private final String urlModePart;

    @Autowired
    public DistanceMatrixRestSender(RestTemplate restTemplate,
                                    @Value("${api.base.url.distance.matrix}") String baseUrlDistanceMatrix,
                                    @Value("${api.key.google.maps}") String apiKeyGoogleUrl,
                                    @Value("${api.base.url.distance.matrix.destination}") String urlDestinationPart,
                                    @Value("${api.base.url.distance.matrix.mode}") String urlModePart) {
        this.restTemplate = restTemplate;
        this.baseUrlDistanceMatrix = baseUrlDistanceMatrix;
        this.apiKeyGoogleUrl = apiKeyGoogleUrl;
        this.urlDestinationPart = urlDestinationPart;
        this.urlModePart = urlModePart;
    }

    String generateMainUrl(Vehicle vehicle, GetDistanceRestCommand getDistanceRestCommand) {
        Stop stop = getDistanceRestCommand.getStop();
        return this.baseUrlDistanceMatrix +
                generateOriginUrl(String.valueOf(stop.getSzerGeo()),
                        String.valueOf(stop.getDlugGeo())) +
                generateDestinationUrl(vehicle) + this.urlModePart + this.apiKeyGoogleUrl;
    }

    String generateOriginUrl(String latitude, String longitude) {
        return latitude + "," + longitude;
    }

    String generateDestinationUrl(Vehicle vehicle) {
        return this.urlDestinationPart + vehicle.getLat() + "," + vehicle.getLon();
    }

    @Override
    public List<DistanceDto> getDistance(GetDistanceRestCommand getDistanceRestCommand) {
        List<DistanceDto> distanceList = new ArrayList<>();
        List<Vehicle> vehicleList = getDistanceRestCommand.getList();
        for (Vehicle vehicle : vehicleList) {
            String url = generateMainUrl(vehicle, getDistanceRestCommand);
            DistanceMatrix distanceMatrix = restTemplate.getForObject(url, DistanceMatrix.class);
            assert distanceMatrix != null;
            Row row = distanceMatrix.getRows().get(0);
            Element element = row.getElements().get(0);
            Distance distance = element.getDistance();
            Duration duration = element.getDuration();
            CreateDistanceCommand createDistanceCommand = CreateDistanceCommand.builder()
                    .originAddress(distanceMatrix.getOriginAddresses().get(0))
                    .destinationAddress(distanceMatrix.getDestinationAddresses().get(0))
                    .time(duration.getText())
                    .timeInSeconds(duration.getValue().toString())
                    .distance(distance.getText())
                    .distanceInUnits(distance.getValue().toString())
                    .build();
            DistanceDto distanceDto = DistanceDtoMapper.distanceDto(createDistanceCommand);
            distanceList.add(distanceDto);
        }
        return distanceList;
    }
}
