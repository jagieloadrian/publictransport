package gitlab.adrian.publictransport.infrastructure.vehicle;

import gitlab.adrian.publictransport.api.vehicle.VehicleRequest;
import gitlab.adrian.publictransport.api.vehicle.VehicleRequestList;
import gitlab.adrian.publictransport.domain.vehicle.VehicleDto;
import gitlab.adrian.publictransport.domain.vehicle.VehicleDtoMapper;
import gitlab.adrian.publictransport.domain.vehicle.VehicleRetrievalRestClient;
import gitlab.adrian.publictransport.domain.vehicle.request.CreateVehicleRequestCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
class VehiclesRestSender implements VehicleRetrievalRestClient {


    private final RestTemplate restTemplate;
    private final String publicTransportBaseUrl;

    @Autowired
    public VehiclesRestSender(RestTemplate restTemplate,
                              @Value("${api.um.publictransport.base.url}") String publicTransportBaseUrl) {
        this.restTemplate = restTemplate;
        this.publicTransportBaseUrl = publicTransportBaseUrl;
    }

    @Override
    public List<VehicleDto> getVehicles(CreateVehicleRequestCommand createVehicleRequestCommand) {
        String url = generateURL(createVehicleRequestCommand.getLine());
        VehicleRequestList vehicles = restTemplate.getForObject(
                url, VehicleRequestList.class);
        assert vehicles != null;
        List<VehicleRequest> result = vehicles.getVehicleRequests();

        return result.stream()
                .map(VehicleDtoMapper::vehicleDto)
                .collect(Collectors.toList());
    }

    String generateURL(String line) {
        String baseURL = publicTransportBaseUrl;
        if (line.length() <= 2) {
            return baseURL + "&type=2" + "&line=" + line;
        }
        return baseURL + "&type=1" + "&line=" + line;
    }
}